// solution insert one

db.users.insert(
    {
     Name : "single",
     accomodates : 2,
     price : 1000,
     description : "A simple room with all the basic necessities",
     rooms_available: 10,
     isAvailable : "false"
 }
 )


 // solution insertMany

 db.users.insertMany([
    {
     Name : "double",
     accomodates : 3,
     price : 2000,
     description : "A room fit for a small family going on a vacation",
     rooms_available: 5,
     isAvailable : "false"
		    },
		    {
     Name : "queen",
     accomodates : 4,
     price : 4000,
     description : "A room with a queen sized bed perfect for a simple getaway",
     rooms_available: 15,
     isAvailable : "false"
		    }
    
 ])

 //Solution Find

 db.users.find({
    
    Name: "double"
    
    })

// Solution Update 

db.users.updateOne(
    { Name: "queen" },
    {
        $set : {
            rooms_available : 0
        }
    }
);

// Solution DeleteMany
db.users.deleteMany({
	rooms_available : 0
});

 